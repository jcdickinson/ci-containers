#!/bin/bash
set -euxo pipefail

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

# Log into GitLab's container repository.
export REGISTRY_AUTH_FILE=${HOME}/auth.json
echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"

# Set up the container's fully qualified name.
FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}"

# Build the container, squash it, and push it to the repository.
buildah bud -f images/${IMAGE_NAME} -t ${IMAGE_NAME} .
CONTAINER_ID=$(buildah from ${IMAGE_NAME})
buildah commit --squash $CONTAINER_ID $FQ_IMAGE_NAME
buildah push ${FQ_IMAGE_NAME}
